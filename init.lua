--[[
███╗   ██╗██╗   ██╗███████╗  ████╗
████╗  ██║██║   ██║██╔════╝ ██  ██╗
██╔██╗ ██║██║   ██║█████╗  ██╗   ██╗
██║╚██╗██║╚██╗ ██╔╝██╔══╝  ██║█████║
██║ ╚████║ ╚████╔╝ ███████╗██║   ██║
╚═╝  ╚═══╝  ╚═══╝  ╚══════╝╚═╝   ╚═╝

Neovim init file
Version: 0.1.5 - 2022/2/21
Maintainer: Li Ran
Website: https://gitee.com/liranux/nvea

--]]

local present, impatient = pcall(require, "impatient")
if present then
  impatient.enable_profile()
end

-----------------------------------------------------------
-- Load theme
-----------------------------------------------------------
local theme_config = require("config").ui
local theme = require("colors")
theme.setup(theme_config)
theme.load()

-----------------------------------------------------------
-- Import Lua modules
-----------------------------------------------------------
require("core")
require("plugins")
require("statusline").setup()

-----------------------------------------------------------
-- Test Lua modules
-----------------------------------------------------------
vim.cmd [[
set rtp+=~/workstation/nvimPlugins/first.nvim
]]

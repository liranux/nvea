local M = {}

---Merge many tables together
---@param... table
---@return table
local function tbl_deep_extend(...)
	local lhs = {}
	for _, rhs in ipairs({ ... }) do
		for k, v in pairs(rhs) do
			if type(lhs[k]) == "table" and type(v) == "table" then
				lhs[k] = tbl_deep_extend(lhs[k], v)
			else
				lhs[k] = v
			end
		end
	end
	return lhs
end

M.config = {
	-- This enables the Neovim background to set either onedark or onelight
	theme = "onedark",
	-- Enable/Disable specific plugins
	plugins = {
		gitsigns = true,
		hop = true,
		indentline = true,
		marks = true,
		native_lsp = true,
		nvim_cmp = true,
		nvim_tree = true,
		nvim_ts_rainbow = true,
		packer = true,
		telescope = true,
		toggleterm = true,
		treesitter = true,
		trouble = true,
		which_key = true,
	},
	options = {
		-- Use a transparent background?
		transparency = false,
		-- Use the theme's colors for Neovim's :terminal?
		terminal_colors = false,
		italic_comment = false,
	},
}

M.set_config = function(user_config)
	user_config = user_config or {}
	M.config = tbl_deep_extend(M.config, user_config)
end

return M

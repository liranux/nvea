local colors = {
  name = "onelight",

  red = "#e05661",
  dark_red = "#BE5046",
  green = "#1da912",
  yellow = "#eea825",
  dark_yellow = "#ee9025",
  blue = "#118dc3",
  purple = "#9a77cf",
  cyan = "#56B6C2",
  gray = "#5C6370",
  white = "#ABB2BF",
  black = "#f0f0f0",
  foreground = "#ABB2BF",
  background = "#f0f0f0",

  comment_grey = "#5C6370",
  gutter_fg_grey = "#4B5263",
  cursor_grey = "#2C323C",
  visual_grey = "#f0f0f0",
  vertsplit = "#f0f0f0",
  extra = {
    teal = "#519ABA",
    pink = "#ff75a0",
    dark_purple = "#8a3fa0",
    dark_cyan = "#2b6f77",
    bg1 = "#31353f",
    bg2 = "#393f4a",
    bg3 = "#3b3f4c",
    bg_d = "#21252b",
    bg_blue = "#73b8f1",
    bg_yellow = "#ebd09c",
    light_grey = "#848b98",
  },
}

return colors

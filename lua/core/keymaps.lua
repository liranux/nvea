local map = require("utils").map
local opration = require("config").basic.opration

-- Don't copy the replaced text after pasting in visual mode
-- 在可视模式下粘贴后 不复制被替换的文本
map("v", "p", '"_dP')
-- Allow moving the cursor through wrapped lines with j, k, <Up> and <Down>
-- 允许光标移动通过折叠行
map("", "j", 'v:count || mode(1)[0:1] == "no" ? "j" : "gj"', { expr = true })
map("", "k", 'v:count || mode(1)[0:1] == "no" ? "k" : "gk"', { expr = true })
map("", "<Down>", 'v:count || mode(1)[0:1] == "no" ? "j" : "gj"', { expr = true })
map("", "<Up>", 'v:count || mode(1)[0:1] == "no" ? "k" : "gk"', { expr = true })
-- use ESC to turn off search highlighting
-- 使用<esc>关闭搜索高亮
map({ "n", "i", "v" }, "<Esc>", "<Esc>:noh <CR>")
-- yank from current cursor to end of line
-- 从当前光标复制到行尾
map("n", "Y", "yg$")
-- Normal和Visual下，使用H, L表示到达行首，行尾
map({ "n", "v" }, "H", "^")
map({ "n", "v" }, "L", "$")
map("n", "n", "nzz")
map("n", "N", "Nzz")
map("n", "*", "*zz")
map("n", "#", "#zz")
map("n", "g*", "g*zz")
-- Visual下，移动当前行
map("v", "J", ":m '>+1<cr>gv=gv")
map("v", "K", ":m '<-2<cr>gv=gv")
map("n", "Q", "@@")

vim.cmd("cnoreabbrev Q q") -- Quit with Q
vim.cmd("cnoreabbrev W w") -- Write with W
vim.cmd("cnoreabbrev WQ wq") -- Write and quit with WQ
vim.cmd("cnoreabbrev Wq wq") -- Write and quit with Wq
vim.cmd("cnoreabbrev Wqa wqa") -- Write and quit all with Wqa
vim.cmd("cnoreabbrev WQa wqa") -- Write and quit all with WQa
vim.cmd("cnoreabbrev WQA wqa") -- Write and quit all with WQA
vim.cmd("cnoreabbrev Wa wa") -- Write all with Wa
vim.cmd("cnoreabbrev WA wa") -- Write all with WA
vim.cmd("cnoreabbrev Qa qa") -- Quit all with Qa
vim.cmd("cnoreabbrev QA qa") -- Quit all with QA
vim.cmd("cnoreabbrev E e") -- Edit file with E
vim.cmd("cnoreabbrev TERM term")
vim.cmd("cnoreabbrev TERm term")
vim.cmd("cnoreabbrev TErm term")
vim.cmd("cnoreabbrev Term term")
vim.cmd("cnoreabbrev work /mnt/e/workstation/")
vim.cmd("cnoreabbrev liran /mnt/c/Users/liran")

map("n", "<leader>e", [[y:lua <c-r>"<cr>]])
map("v", "<leader>e", [[:lua <c-r>"<cr>]])
map("n", ";s", [[:%s/\<<C-R>=expand("<cword>")<CR>\>/]])
map("v", ";s", [[y:%s/\<<C-R>"\>/]])
map("v", ";/", [[y/<c-r>"<cr>]])

-- navigation within insert mode
-- 快速在Insert下移动
if opration.insert then
	map("i", "<C-h>", "<Left>")
	map("i", "<C-e>", "<End>")
	map("i", "<C-l>", "<Right>")
	map("i", "<C-k>", "<Up>")
	map("i", "<C-i>", "<Down>")
	map("i", "<C-a>", "<ESC>I")
end
-- easier navigation between windows
-- 快速在窗口中移动或者改变大小
if opration.window then
	map({ "n", "v" }, "<leader>h", "<C-w>h")
	map({ "n", "v" }, "<leader>j", "<C-w>j")
	map({ "n", "v" }, "<leader>k", "<C-w>k")
	map({ "n", "v" }, "<leader>l", "<C-w>l")

	map({ "n", "v" }, "A-k", "<C-w>+")
	map({ "n", "v" }, "A-j", "<C-w>-")
	map({ "n", "v" }, "A-h", "<C-w>>")
	map({ "n", "v" }, "A-l", "<C-w><")
end

-- map("n", maps.copy_whole_file, ":%y+ <CR>") -- copy whole file content
map("n", "<leader>w", ":wqa <CR>") -- new buffer
map("n", "<leader>nb", ":enew <CR>") -- new buffer
map("n", "<leader>nt", ":tabnew <CR>") -- new tabs
map("n", "<F2>", ":set nu! <CR>") -- toggle numbers
map("n", "<leader>bo", ":%bd|e#|bd#<cr>")
map("i", "<A-;>", "<cmd>lua require('utils').EscapePair()<CR>")

-- bufferline()
map("n", "<Tab>", ":BufferLineCycleNext <CR>")
map("n", "<S-Tab>", ":BufferLineCyclePrev <CR>")

-- nvimtree()
map("n", "<localleader>e", ":NvimTreeToggle <CR>")
map("n", "<leader>o", ":NvimTreeFocus <CR>")

vim.cmd([[ command! Format execute 'lua vim.lsp.buf.formatting()']])

-- Hop
map({ "n", "v" }, "<localleader>'", "<cmd>HopWord<cr>")
map({ "n", "v" }, "gl", "<cmd>HopLine<cr>")
map({ "n", "v" }, "gs", "<cmd>HopChar1<cr>")

map("n", "<leader>gj", "<cmd>lua require 'gitsigns'.next_hunk()<cr>")
map("n", "<leader>gk", "<cmd>lua require 'gitsigns'.prev_hunk()<cr>")
map("n", "<leader>gl", "<cmd>lua require 'gitsigns'.blame_line()<cr>")
map("n", "<leader>gp", "<cmd>lua require 'gitsigns'.preview_hunk()<cr>")
map("n", "<leader>gr", "<cmd>lua require 'gitsigns'.reset_hunk()<cr>")
map("n", "<leader>gR", "<cmd>lua require 'gitsigns'.reset_buffer()<cr>")
map("n", "<leader>gs", "<cmd>lua require 'gitsigns'.stage_hunk()<cr>")
map("n", "<leader>gu", "<cmd>lua require 'gitsigns'.undo_stage_hunk()<cr>")
map("n", "<leader>go", "<cmd>Telescope git_status<cr>")
map("n", "<leader>gb", "<cmd>Telescope git_branches<cr>")
map("n", "<leader>gc", "<cmd>Telescope git_commits<cr>")
map("n", "<leader>gd", "<cmd>Gitsigns diffthis HEAD<cr>")

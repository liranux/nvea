local opt = vim.opt
local g = vim.g
g.mapleader = ','
g.maplocalleader = ' '
opt.signcolumn = "yes"
-- when cursor reaches end/beginning of line
opt.whichwrap:append "<>[]hl"
-- disable nvim intro
opt.shortmess:append "sI"
opt.fileencodings = "utf-8,ucs-bom,gb18030,gbk,gb2312,cp936,latin1"

opt.number = true
opt.numberwidth = 2
opt.cursorline = true
opt.mouse = 'a'
opt.clipboard = 'unnamedplus'
opt.cmdheight = 1

opt.shiftwidth = 4
opt.tabstop = 4
opt.softtabstop = 4
opt.smartcase = true
opt.smarttab = true
opt.expandtab = true
opt.smartindent = true

opt.scrolloff = 5
opt.showmatch = true
opt.splitright = true
opt.splitbelow = true
opt.swapfile = false
opt.list = true
opt.listchars = "tab:»·,space:·,nbsp:+,trail:·,extends:→,precedes:←"

opt.ignorecase = true
opt.smartcase = true
opt.breakindent = true
opt.completeopt = "menuone,noselect"

-- 折叠
vim.wo.foldmethod = "expr"
vim.wo.foldexpr = "nvim_treesitter#foldexpr()"
vim.wo.foldlevel = 99


-- time
vim.o.updatetime = 100
-----------------------------------------------------------
-- Colorscheme
-----------------------------------------------------------
opt.termguicolors = true

-----------------------------------------------------------
-- Defaults
-----------------------------------------------------------
-- g.encoding = "UTF-8"
-- opt.ruler = true
-- opt.background = "dark"
-- opt.hidden = true
-- opt.hlsearch = true
-- opt.incsearch = true

-----------------------------------------------------------
-- Disable some builtin vim plugins
-----------------------------------------------------------
local disabled_built_ins = {
   "2html_plugin",
   "getscript",
   "getscriptPlugin",
   "gzip",
   "logipat",
   "netrw",
   "netrwPlugin",
   "netrwSettings",
   "netrwFileHandlers",
   "matchit",
   "tar",
   "tarPlugin",
   "rrhelper",
   "spellfile_plugin",
   "vimball",
   "vimballPlugin",
   "zip",
   "zipPlugin",
}
for _, plugin in pairs(disabled_built_ins) do
   g["loaded_" .. plugin] = 1
end

-----------------------------------------------------------
-- Others
-----------------------------------------------------------
opt.shadafile = ""
opt.shell = "/bin/zsh"
opt.lazyredraw = true
g.python3_host_prog = '/bin/python3'

-----------------------------------------------------------
-- Wsl clipboard
-- ensure win32yank.exe in your PATH
-- https://github.com/neovim/neovim/wiki/FAQ#how-to-use-the-windows-clipboard-from-wsl
-----------------------------------------------------------
local clipboard_settings = function()
    vim.cmd [[
    let g:clipboard = {
          \   'name': 'win32yank-wsl',
          \   'copy': {
          \      '+': 'win32yank.exe -i --crlf',
          \      '*': 'win32yank.exe -i --crlf',
          \    },
          \   'paste': {
          \      '+': 'win32yank.exe -o --lf',
          \      '*': 'win32yank.exe -o --lf',
          \   },
          \   'cache_enabled': 0,
          \ }
    ]]
end
if vim.fn.has('wsl') then
  clipboard_settings()
end

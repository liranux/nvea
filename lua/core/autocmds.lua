local cmd = vim.cmd
-- Compile code
-- Get current filetype -> :echo &filetype or as variable &filetypes
-- [ Builds / Compiles / Interpretes  ]
cmd([[
augroup bci
  autocmd FileType c nnoremap <buffer> <leader>r :5sp term://gcc % && ./a.out <CR>
  autocmd FileType cpp nnoremap <buffer> <leader>r :5sp term://g++ % && ./a.out <CR>
  autocmd FileType python nnoremap <buffer> <leader>r :5sp term://python % <CR>
  autocmd FileType sh nnoremap <buffer> <leader>r :5sp term://sh % <CR>
  autocmd FileType markdown nnoremap <buffer> <leader>r :MarkdownPreview <CR>
  autocmd FileType vim,zsh,tmux,lua nnoremap <buffer> <leader>r :source % <CR>
augroup END
]])

-- Don't show any numbers inside terminals
-- cmd [[ autocmd TermOpen term://* setlocal nonumber norelativenumber | setfiletype terminal ]]

-- File extension specific tabbing
cmd([[ autocmd Filetype lua,lisp setlocal expandtab tabstop=2 shiftwidth=2 softtabstop=2 ]])

-- Open a file from its last left off position
cmd(
	[[ autocmd BufReadPost * if expand('%:p') !~# '\m/\.git/' && line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif ]]
)

-- remove trailing whitespaces
cmd([[autocmd BufWritePre * %s/\s\+$//e]])

-- remove trailing newline
cmd([[autocmd BufWritePre * %s/\n\+\%$//e]])

-- yank highlight
cmd([[autocmd TextYankPost * silent! lua vim.highlight.on_yank({higroup="IncSearch", timeout=300})]])

function _G.set_terminal_keymaps()
	local opts = { noremap = true, silent = true }
	vim.api.nvim_set_keymap("t", "<esc>", [[<C-\><C-n>]], opts)
	vim.api.nvim_set_keymap("t", "jk", [[<C-\><C-n>]], opts)
	vim.api.nvim_set_keymap("t", "<C-h>", [[<C-\><C-n><C-W>h]], opts)
	vim.api.nvim_set_keymap("t", "<C-j>", [[<C-\><C-n><C-W>j]], opts)
	vim.api.nvim_set_keymap("t", "<C-k>", [[<C-\><C-n><C-W>k]], opts)
	vim.api.nvim_set_keymap("t", "<C-l>", [[<C-\><C-n><C-W>l]], opts)
end
vim.cmd("autocmd! TermOpen term://* lua set_terminal_keymaps()")

-- cmd([[
-- augroup illuminate_augroup
--     autocmd!
--     autocmd VimEnter * hi illuminatedWord cterm=underline gui=underline
-- augroup END
-- ]])

-- format files on save
-- cmd([[
--   augroup LspFormatting
--     autocmd! * <buffer>
--     autocmd BufWritePre <buffer> lua vim.lsp.buf.formatting_sync()
--   augroup END
-- ]])

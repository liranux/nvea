local modules = {
  "core.default",
  "core.autocmds",
  "core.keymaps",
}

for _, module in ipairs(modules) do
  local ok, err = pcall(require, module)
  if not ok then
    print("Error loading " .. module .. "\n\n" .. err)
  end
end

local home    = os.getenv("HOME")
local os_name = vim.loop.os_uname().sysname
local is_mac = "Darwin" == os_name
local is_linux = 'Linux' == os_name
local is_windows = 'Windows' == os_name
local is_wsl = (function()
  local output = vim.fn.systemlist "uname -r"
  return not not string.find(output[1] or "", "WSL")
end)()
local path_sep = is_windows and '\\' or '/'

return {
  home = home,
  os_name = os_name,
  is_linux = is_linux,
  is_windows= is_windows,
  is_mac = is_mac,
  is_wsl = is_wsl,
  path_sep = path_sep,

  nvim_config = vim.fn.stdpath("config"),
  nvim_cache = home .. path_sep .. ".cache" .. path_sep.. "nvim",
  nvim_data = vim.fn.stdpath("data"),
  nvim_site = vim.fn.stdpath("data") .. path_sep .. "site",
}

local comp = require("statusline.components")

local M = {}

M.active = function()
	return table.concat({
		-- "%#Red#",
		comp.update_mode_colors(),
		comp.mode(),
		"%=",
		comp.filename(),
		comp.filesize(),
		"%=",
		comp.location(),
		comp.encoding(),
	})
end
function M.inactive()
	return "%= %t %="
end
function M.short()
	return " NvimTree"
end


M.set_statusline = function()
  vim.opt.statusline = [[%{%v:lua.require("statusline").active()%}]]
end

M.setup = function() end
M.set_statusline()

return M

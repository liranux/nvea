local M = {}

local colors = {
  active = "%#StatusLine#",
  mode = "%#Mode#",
}

local modes = {
	["n"] = "NORMAL",
	["no"] = "NORMAL",
	["v"] = "VISUAL",
	["V"] = "VISUAL LINE",
	[""] = "VISUAL BLOCK",
	["s"] = "SELECT",
	["S"] = "SELECT LINE",
	[""] = "SELECT BLOCK",
	["i"] = "INSERT",
	["ic"] = "INSERT",
	["R"] = "REPLACE",
	["Rv"] = "VISUAL REPLACE",
	["c"] = "COMMAND",
	["cv"] = "VIM EX",
	["ce"] = "EX",
	["r"] = "PROMPT",
	["rm"] = "MOAR",
	["r?"] = "CONFIRM",
	["!"] = "SHELL",
	["t"] = "TERMINAL",
}

M.mode = function()
	local current_mode = vim.api.nvim_get_mode().mode
	return string.format(" %s ", modes[current_mode]):upper()
end


M.update_mode_colors =function ()
  local current_mode = vim.api.nvim_get_mode().mode
  local mode_color = "%#StatusLineCommon#"
  if current_mode == "n" or current_mode == "no" then
    mode_color = "%#StatuslineCommon#"
  elseif current_mode == "i" or current_mode == "ic" then
    mode_color = "%#StatuslineInsert#"
  elseif current_mode == "v" or current_mode == "V" or current_mode == "" then
    mode_color = "%#StatuslineVisual#"
  elseif current_mode == "R" or current_mode == "Rv" then
    mode_color = "%#StatuslineReplace#"
  elseif current_mode == "c" then
    mode_color = "%#StatuslineCmd#"
  elseif current_mode == "t" then
    mode_color = "%#StatuslineTerminal#"
  end
  return mode_color
end

M.filename = function()
	local fname = vim.fn.expand("%:t")
  if fname == "" then
    return "No Name"
  end
	return string.format(" %s ", fname)
end

M.filesize = function()
	local file = vim.fn.expand("%:p")
	if file == nil or #file == 0 then
		return ""
	end

	local size = vim.fn.getfsize(file)
	if size <= 0 then
		return ""
	end

	local sufixes = { "b", "k", "m", "g" }

	local i = 1
	while size > 1024 and i < #sufixes do
		size = size / 1024
		i = i + 1
	end

	return string.format(" %.1f%s ", size, sufixes[i])
end

M.encoding = function ()
  return string.format(" %s ", vim.opt.fileencoding:get())
end

M.location = function ()
  if vim.bo.filetype == "alpha" then
    return ""
  end
  return " %l:%v %3p%% "
end

return M

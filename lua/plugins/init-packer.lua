local g = require("core.global")
local sep = g.path_sep
local site = g.nvim_site
local fn = vim.fn
local first_install
local packer_path = site .. sep .. "pack" .. sep .. "packer" .. sep .. "start" .. sep .. "packer.nvim"

if fn.empty(fn.glob(packer_path)) > 0 then
  print("Cloning packer..")
  vim.fn.delete(packer_path, "rf")
  first_install = vim.fn.system({
    "git",
    "clone",
    "--depth",
    "1",
    "git://github.com/wbthomason/packer.nvim",
    packer_path,
  })
end

-- vim.cmd "packadd packer.nvim"
local state, packer = pcall(require, "packer")
if not state then
  vim.notify("Packer.nvim installation failed.", vim.log.levels.ERROR)
  return
end

packer.init({
	max_job = 16,
	display = {
		open_fn = function()
			return require("packer.util").float({ border = "single" })
		end,
		working_sym = "⁘",
		error_sym = "×",
		done_sym = "✓",
		removed_sym = "!!",
		moved_sym = "!",
		header_sym = "━",
		show_all_info = true,
		prompt_border = "single",
	},
	git = {
		clone_timeout = 1000,
		default_url_format = "git://github.com/%s",
		-- default_url_format = 'https://hub.fastgit.org/%s',
	},
	auto_clean = true,
	compile_on_sync = true,
})

return {
	packer = packer,
	first_install = first_install,
}

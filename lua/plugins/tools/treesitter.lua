local status, ts_config = pcall(require, "nvim-treesitter.configs")
if not status then
	return print("Nvim-treesitter not found!!!")
end
-- vim.cmd[[packadd nvim-treesitter-refactor]]
-- vim.cmd[[packadd nvim-treesitter-textobjects]]
-- vim.cmd[[packadd nvim-ts-rainbow]]

require("nvim-treesitter.install").prefer_git = true
local parser_config = require("nvim-treesitter.parsers").get_parser_configs()
for _, value in pairs(parser_config) do
	value.install_info.url = value.install_info.url:gsub("https", "git")
end

ts_config.setup({
	ensure_installed = {
		"lua",
		"python",
		"c",
		"bash",
		"cpp",
		"java",
		"json",
		"markdown",
	},
	sync_install = true,
	highlight = {
		enable = true,
		additional_vim_regex_highlighting = true,
	},
	-- 增量选择
	incremental_selection = {
		enable = true,
		keymaps = {
			init_selection = "<cr>",
			node_incremental = "<cr>",
			node_decremental = "<bs>",
			scope_incremental = ",s",
		},
	},
	-- Indentation based on treesitter for the = operator
	indent = {
		enable = true,
	},
	refactor = {
		smart_rename = {
			enable = true,
			keymaps = {
				smart_rename = "grr",
			},
		},
	},
	rainbow = {
		enable = true,
		extended_mode = true,
		max_file_lines = nil,
		-- disable = { "jsx", "cpp" }
		-- colors = {}, -- table of hex strings
		-- termcolors = {} -- table of colour name strings
	},
})

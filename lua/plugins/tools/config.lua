local M = {}
M.context = function()
	local status, context = pcall(require, "treesitter-context")
	if not status then
		return print("Treesitter-context not found!")
	end
	context.setup({})
end

M.markdown_preview = function()
	vim.cmd("let g:mkdp_browser = 'C:\\Program Files\\Google\\Chrome\\Application\\chrome.exe'")
end

M.colorizer = function()
	local state, colorizer = pcall(require, "colorizer")
	if not state then
		return print("Colorizer not present!!!")
	end
	colorizer.setup()
end

M.neoscroll = function()
	local state, scroll = pcall(require, "neoscroll")
	if not state then
		return print("Neoscroll not present!!!")
	end
	scroll.setup({
		-- All these keys will be mapped to their corresponding default scrolling animation
		mappings = {
			"<C-u>",
			"<C-d>",
			"<C-b>",
			"<C-f>",
			"<C-y>",
			"<C-e>",
			"zt",
			"zz",
			"zb",
		},
		hide_cursor = true, -- Hide cursor while scrolling
		stop_eof = true, -- Stop at <EOF> when scrolling downwards
		use_local_scrolloff = false, -- Use the local scope of scrolloff instead of the global scope
		respect_scrolloff = false, -- Stop scrolling when the cursor reaches the scrolloff margin of the file
		cursor_scrolls_alone = true, -- The cursor will keep on scrolling even if the window cannot scroll further
		easing_function = nil, -- Default easing function
		pre_hook = nil, -- Function to run before the scrolling animation starts
		post_hook = nil, -- Function to run after the scrolling animation ends
	})
end

return M

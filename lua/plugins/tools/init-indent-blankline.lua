local present, blankline = pcall(require, "indent_blankline")
if not present then
  return print("Indent_Blankline not present!!!")
end
blankline.setup {
  -- char = "┊",
  char = "│",
  char_highlight_list = {
    "IndentBlanklineIndent1",
    "IndentBlanklineIndent2",
    "IndentBlanklineIndent3",
    "IndentBlanklineIndent4",
    "IndentBlanklineIndent5",
    "IndentBlanklineIndent6",
  },
  filetype_exclude = {
    "help",
    "terminal",
    "dashboard",
    "packer",
    "lspinfo",
    "TelescopePrompt",
    "TelescopeResults",
    "notification",
    "cmenu",
    "lsp-installer",
  },
  buftype_exclude = {
    "terminal",
  },
  -- show_current_context = true,
  -- show_current_context_start = true,  -- underline first line
  use_treesitter = true,
}

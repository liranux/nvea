local get_conf = function(plug)
	if type(plug) ~= "string" then
		return print("function conf need a string para")
	end
	if plug == "config" then
		return require("plugins.tools.config")
	end
	return string.format('require("plugins.tools.%s")', plug)
end

return function(use)
	-- -------------------------------- Looks ------------------------------- --
	use({ "kyazdani42/nvim-web-devicons" })
	use({
		"nvim-treesitter/nvim-treesitter",
		run = ":TSUpdate",
		config = get_conf("treesitter"),
		requires = {
			{ "nvim-treesitter/nvim-treesitter-refactor", after = "nvim-treesitter" },
			{ "romgrk/nvim-treesitter-context", after = "nvim-treesitter" },
		},
	})
	use({
		"p00f/nvim-ts-rainbow",
		after = "nvim-treesitter",
	})
	use({
		"lukas-reineke/indent-blankline.nvim",
		event = { "BufRead", "BufNewFile" },
		config = get_conf("init-indent-blankline"),
	})

	-- ------------------------- Tree, Buffer, Statusline ------------------------- --
	use({
		"kyazdani42/nvim-tree.lua",
		cmd = { "NvimTreeToggle", "NvimTreeFocus" },
		config = get_conf("nvimtree"),
	})
	use({
		"akinsho/bufferline.nvim",
		after = "nvim-web-devicons",
		config = get_conf("init-bufferline"),
	})
	use({
		"nvim-lualine/lualine.nvim",
		after = "nvim-web-devicons",
		-- config = get_conf("lualine"),
	})

	-- ------------------------------ Telescope ----------------------------- --
	use({
		"nvim-telescope/telescope.nvim",
    event = "BufRead",
		config = get_conf("init-telescope"),
	})

	-- ------------------------------ Git ----------------------------- --
	use({
		"lewis6991/gitsigns.nvim",
		event = "BufRead",
		config = get_conf("init-gitsigns"),
	})
	use({ "sindrets/diffview.nvim", cmd = { "DiffviewOpen" } })

	-- ------------------------------ Markdown ----------------------------- --
	use({
		"iamcco/markdown-preview.nvim",
		run = "cd app && yarn install",
		ft = "markdown",
		cmd = "MarkdownPreview",
		config = get_conf("config").markdown_preview,
	})

	-- ------------------------------ Tools ----------------------------- --
	use({
		"folke/which-key.nvim",
		config = get_conf("whichkey"),
	})
	use({
		"folke/trouble.nvim",
		after = "nvim-web-devicons",
		cmd = { "TroubleToggle", "Trouble" },
		-- config = conf("trouble")
	})
	use({
		"karb94/neoscroll.nvim",
		event = "BufRead",
		config = get_conf("config").neoscroll,
	})
	use({
		"folke/todo-comments.nvim",
		config = get_conf("init-todo"),
		cmd = { "TodoQuickFix", "TodoTrouble", "TodoTelescope" },
	})
	use({
		"norcalli/nvim-colorizer.lua",
		cmd = { "ColorizerToggle" },
		config = get_conf("config").colorizer,
	})

	-- ------------------------------ Helpful tools ----------------------------- --
	use({ "nathom/filetype.nvim" })
	use({
		"luukvbaal/stabilize.nvim",
		event = "BufRead",
		config = function()
			require("stabilize").setup()
		end,
	})
	use({
		"dstein64/vim-startuptime",
		cmd = "StartupTime",
		config = function()
			vim.g.startuptime_tries = 10
		end,
	})
	use({ "MunifTanjim/nui.nvim" })
	use({
		"nacro90/numb.nvim",
		event = "BufRead",
		config = function()
			require("numb").setup({
				show_numbers = true, -- Enable 'number' for the window while peeking
				show_cursorline = true, -- Enable 'cursorline' for the window while peeking
			})
		end,
	})
	use({
		"j-hui/fidget.nvim",
		config = function()
			require("fidget").setup({
				text = {
					spinner = "moon",
				},
			})
		end,
	})
end

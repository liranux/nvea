local present, which_key = pcall(require, "which-key")
if not present then
	return print("Whichkey not present!!!")
end
which_key.setup({
	plugins = {
		marks = false,
		registers = false,
		spelling = {
			enabled = true,
			suggestions = 20,
		},
		presets = {
			operators = false,
			motions = false,
			text_objects = false,
			windows = true,
			nav = false,
			z = true,
			g = true,
		},
	},
	key_labels = {
		["<space>"] = "SPC",
		["<cr>"] = "RET",
		["<tab>"] = "TAB",
	},
	window = {
		border = "single", -- none, single, double, shadow
		position = "bottom", -- bottom, top
		margin = { 1, 0, 1, 0 }, -- extra window margin [top, right, bottom, left]
		padding = { 2, 2, 2, 2 }, -- extra window padding [top, right, bottom, left]
		winblend = 0,
	},
	layout = {
		height = { min = 4, max = 25 }, -- min and max height of the columns
		width = { min = 20, max = 50 }, -- min and max width of the columns
		spacing = 3, -- spacing between columns
		align = "left", -- align columns left, center or right
	},
	ignore_missing = true,
	hidden = { "<silent>", "<cmd>", "<Cmd>", "<CR>", "call", "lua", "^:", "^ ", "@" },
	show_help = true,
	triggers = "auto",
	triggers_blacklist = {
		-- list of mode / prefixes that should never be hooked by WhichKey
		-- this is mostly relevant for key maps that start with a native binding
		-- most people should not need to change this
		i = { "j", "k" },
		v = { "j", "k" },
	},
})
local space = {
	mode = "n",
	prefix = "<localleader>",
	buffer = nil,
	silent = true,
	noremap = true,
	nowait = true,
}
local map_space = {
  ["f"] = "Find Files",
  ["b"] = "Find Buffer",
  ["h"] = "Historical Files",
  ["s"] = "Search Words",
  ["'"] = "Go Words",
  ["e"] = "Explorer",
}

local comma = {
	mode = "n",
	prefix = "<leader>",
	buffer = nil,
	silent = true,
	noremap = true,
	nowait = true,
}
local map_comma = {
  ["w"] = "Save and Quit",
	["o"] = { "Focu File On Explorer" },
  ["h"] = "Help",
  n = {
    name = "New",
    b = "New Buffer",
    t = "New Tabs",
  },
	p = {
		name = "Packer",
		c = { "<cmd>PackerCompile<cr>", "Compile" },
		C = { "<cmd>PackerClean<cr>", "Clean" },
		i = { "<cmd>PackerInstall<cr>", "Install" },
		S = { "<cmd>PackerSync<cr>", "Sync" },
		s = { "<cmd>PackerStatus<cr>", "Status" },
		u = { "<cmd>PackerUpdate<cr>", "Update" },
	},
	g = {
		name = "Git",
		j = { "Next Hunk" },
		k = { "Prev Hunk" },
		l = { "Blame" },
		p = { "Preview Hunk" },
		r = { "Reset Hunk" },
		R = { "Reset Buffer" },
		s = { "Stage Hunk" },
		u = { "Undo Stage Hunk" },
		o = { "Open changed file" },
		b = { "Checkout branch" },
		c = { "Checkout commit" },
		d = { "Diff" },
	},
	l = {
		name = "LSP",
		a = { "Code Action" },
		r = { "Rename" },
		d = { "Document Diagnostics" },
		w = { "Workspace Diagnostics" },
		e = { "Show Line Diagnostics" },
		i = { "Info" },
		I = { "Installer Info" },
		j = { "Next Diagnostic" },
		k = { "Prev Diagnostic" },
		l = { "Quickfix" },
		s = { "Signaturehelp Help" },
	},
	f = {
		name = "Find ",
		l = { "Builtin" },
		f = { "Find File" },
		a = { "Find Hide File" },
		b = { "Find Buffer" },
		w = { "Find Text" },
		s = { "Colorscheme" },
		H = { "Find Help" },
		h = { "Open Recent File" },
		R = { "Registers" },
		k = { "Keymaps" },
		c = { "Commands" },
		C = { "Commands History" },
	},
	b = {
		name = "Diagnostics",
		t = { "<cmd>TroubleToggle<cr>", "trouble" },
		w = { "<cmd>TroubleToggle workspace_diagnostics<cr>", "workspace" },
		d = { "<cmd>TroubleToggle document_diagnostics<cr>", "document" },
		q = { "<cmd>TroubleToggle quickfix<cr>", "quickfix" },
		l = { "<cmd>TroubleToggle loclist<cr>", "loclist" },
		r = { "<cmd>TroubleToggle lsp_references<cr>", "references" },
	},
}
local nop_mappings = {
	["<F2>"] = { "Toggle number" },
	["<A-h>"] = { "Increase Width" },
	["<A-j>"] = { "Decrease Height" },
	["<A-k>"] = { "Increase Height" },
	["<A-l>"] = { "Decrease Width" },
	["#"] = { "Search Up for the current word" },
	["*"] = { "Search Down for the current word" },
  g = {
		name = "Easy Motion",
		l = { "Go Line" },
		s = { "Go Char" },
  }
}
which_key.register(map_space, space)
which_key.register(map_comma, comma)
which_key.register(nop_mappings)

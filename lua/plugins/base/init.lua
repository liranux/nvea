local get_conf = require("plugins.base.config")

return function(use)
	use({
		"numToStr/Comment.nvim",
		config = get_conf.comment,
	})
	use({
		"windwp/nvim-autopairs",
		config = get_conf.autopairs,
	})
	use({
		"mg979/vim-visual-multi",
		branch = "master",
		keys = "<C-n>",
	})
	use({
		"liuchengxu/vista.vim",
		cmd = "Vista",
	})
	use({
		"phaazon/hop.nvim",
		branch = "v1",
		cmd = { "HopWord", "HopLine", "HopChar1" },
		config = get_conf.hop,
	})
	use({
		"tpope/vim-surround",
		keys = {
			{ "n", "cs" },
			{ "n", "ds" },
			{ "x", "S" },
			{ "n", "ys" },
		},
	})
end

local M = {}
M.comment = function()
	local status, comment = pcall(require, "Comment")
	if not status then
		return print("Comment not found!")
	end
	comment.setup()
end

M.hop = function()
	local status, Hop = pcall(require, "hop")
	if not status then
		return print("Hop not found!")
	end
	Hop.setup({ keys = "asdfghjklqwertyuiop" })
end

M.autopairs = function()
	local statue, npairs = pcall(require, "nvim-autopairs")
	if not statue then
		return print("Autopairs not present!!!")
	end
	npairs.setup({
		check_ts = true,
		enable_check_bracket_line = false,
		fast_wrap = {
			map = "<A-e>",
			chars = { "{", "[", "(", '"', "'" },
			pattern = string.gsub([[ [%'%"%)%>%]%)%}%,] ]], "%s+", ""),
			offset = 0, -- Offset from pattern match
			end_key = "$",
			keys = "asdfghjkl",
			check_comma = true,
			highlight = "Search",
			highlight_grey = "Comment",
		},
	})
end

return M

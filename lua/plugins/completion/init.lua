return function(use)
	use({
		"L3MON4D3/LuaSnip",
    config = [[require("plugins.completion.luasnip")]],
		requires = { "rafamadriz/friendly-snippets", after = "LuaSnip" },
	})
	use({
		"hrsh7th/nvim-cmp",
		after = "LuaSnip",
		config = [[require("plugins.completion.nvim-cmp")]],
		requires = {
			{ "hrsh7th/cmp-nvim-lsp", after = "nvim-cmp" },
			{ "saadparwaiz1/cmp_luasnip", after = "nvim-cmp" },
			{ "hrsh7th/cmp-buffer", after = "nvim-cmp" },
			{ "hrsh7th/cmp-nvim-lua", after = "nvim-cmp" },
			{ "hrsh7th/cmp-path", after = "nvim-cmp" },
			{ "hrsh7th/cmp-cmdline", after = "cmp-path" },
		},
		-- event = "InsertEnter",
	})
	use({
		"uga-rosa/cmp-dictionary",
		after = "nvim-cmp",
		config = [[require("plugins.completion.cmp-dic")]],
	})
end

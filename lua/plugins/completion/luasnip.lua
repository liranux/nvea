local status_luasnip, luasnip = pcall(require, "luasnip")
if not status_luasnip then
	return print("Luasnip not found!!!")
end

luasnip.config.set_config({
  history = true,
  updateevents = "TextChanged,TextChangedI",
})
-- require("luasnip.loaders.from_vscode").lazy_load({paths={"~/.config/nvim/my-snippets"}})
require("luasnip.loaders.from_vscode").lazy_load()

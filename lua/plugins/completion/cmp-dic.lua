local status, cmp_dic = pcall(require, "cmp_dictionary")
if not status then
  return print("Cmp_dic not found!!!")
end
cmp_dic.setup({
    dic = {
        ["*"] = "~/.config/nvim/dict/words",
        -- ["markdown"] = { "path/to/mddict", "path/to/mddict2" },
        -- ["javascript,typescript"] = { "path/to/jsdict" },
    },
})

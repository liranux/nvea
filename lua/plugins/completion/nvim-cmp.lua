local status, cmp = pcall(require, "cmp")
if not status then
	return print("Cmp not found!!!")
end

local status_luasnip, luasnip = pcall(require, "luasnip")
if not status_luasnip then
	return print("Luasnip not found!!!")
end

local load_cmp_autopairs = function()
	local cmp_autopairs = require("nvim-autopairs.completion.cmp")
	cmp.event:on("confirm_done", cmp_autopairs.on_confirm_done())
end
load_cmp_autopairs()

local lspkind_icons = {
	Text = "",
	Method = "",
	Function = "",
	Constructor = "",
	Field = "",
	Variable = "",
	Class = "ﴯ",
	Interface = "",
	Module = "",
	Property = "ﰠ",
	Unit = "",
	Value = "",
	Enum = "",
	Keyword = "",
	Snippet = "",
	Color = "",
	File = "",
	Reference = "",
	Folder = "",
	EnumMember = "",
	Constant = "",
	Struct = "",
	Event = "",
	Operator = "",
	TypeParameter = "",
}

local has_words_before = function()
	local line = vim.api.nvim_win_get_cursor(0)[1]
	local col = vim.api.nvim_win_get_cursor(0)[2]
	return col ~= 0 and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match("%s") == nil
end

cmp.setup({
	snippet = {
		expand = function(args)
			require("luasnip").lsp_expand(args.body)
		end,
	},
	formatting = {
		fields = { "kind", "abbr", "menu" },
		format = function(entry, vim_item)
			vim_item.kind = string.format("%s", lspkind_icons[vim_item.kind])
			--vim_item.menu = nil
			vim_item.menu = ({
				buffer = "﬘",
				nvim_lsp = "🎈",
				dictionary = "暈",
				luasnip = "",
				nvim_lua = "",
			})[entry.source.name]
			return vim_item
		end,
	},
	mapping = {
		["<CR>"] = cmp.mapping.confirm({ select = true }),
		["<C-p>"] = cmp.mapping.select_prev_item(),
		["<C-n>"] = cmp.mapping.select_next_item(),
		["<C-b>"] = cmp.mapping(cmp.mapping.scroll_docs(-1), { "i", "c" }),
		["<C-f>"] = cmp.mapping(cmp.mapping.scroll_docs(1), { "i", "c" }),
		["<C-e>"] = cmp.mapping({
			i = cmp.mapping.abort(),
			c = cmp.mapping.close(),
		}),
    ["<A-k>"] = cmp.mapping.complete({
      config = {
        sources = {
          { name = "dictionary", Keyword_length = 2 },
        },
      },
    }),
    ["<Tab>"] = cmp.mapping(function(fallback)
			if cmp.visible() then
				cmp.select_next_item()
			elseif luasnip.expandable() then
				luasnip.expand()
			elseif luasnip.expand_or_jumpable() then
				luasnip.expand_or_jump()
			elseif has_words_before() then
				cmp.complete()
			else
				fallback()
			end
		end, { "i", "s" }),
		["<S-Tab>"] = cmp.mapping(function(fallback)
			if cmp.visible() then
				cmp.select_prev_item()
			elseif luasnip.jumpable(-1) then
				luasnip.jump(-1)
			else
				fallback()
			end
		end, { "i", "s" }),
	},
	sources = cmp.config.sources({
		{ name = "nvim_lsp" },
		{ name = "luasnip" },
		{ name = "path" },
		{
			name = "buffer",
			option = {
				get_bufnrs = function()
					return vim.api.nvim_list_bufs()
				end,
			},
		},
	}),
	documentation = {
		border = "single",
		winhighlight = "normalfloat:cmpdocumentation,floatborder:cmpdocumentationborder",
	},
})

cmp.setup.cmdline("/", {
	-- completion = {
	-- 	autocomplete = false,
	-- },
	view = {
		entries = { name = "wildmenu", separator = "|" },
	},
	sources = {
		{
			name = "buffer",
			option = {
				get_bufnrs = function()
					return vim.api.nvim_list_bufs()
				end,
			},
		},
	},
})
cmp.setup.cmdline(":", {
	-- completion = {
	-- 	autocomplete = false,
	-- },
	formatting = {
		format = function(_, vim_item)
			vim_item.kind = ""
			vim_item.menu = nil
			return vim_item
		end,
	},
	sources = {
		{ name = "path" },
		{ name = "cmdline" },
	},
})

return function(use)
	use({
		"neovim/nvim-lspconfig",
		ft = { "python", "lua", "sh", "c", "cpp" },
		config = [[require("plugins.lsp.lspconfig")]],
	})
	use({
		"williamboman/nvim-lsp-installer",
		after = "nvim-lspconfig",
		config = [[require("plugins.lsp.lsp_installer")]],
	})
	use({
		"jose-elias-alvarez/null-ls.nvim",
		event = "BufRead",
		config = [[require("plugins.lsp.null_ls")]],
	})
end

local status, nvim_lsp = pcall(require, "lspconfig")
if not status then
	vim.notify("Lsp not found!", vim.log.levels.ERROR)
	return
end

-- beauty
require("plugins.lsp.handlers")()
local on_attach = require("plugins.lsp.on_attach")
local capabilities = require("plugins.lsp.capabilities")

-- 安装pyright
-- npm i -g pyright
-- 将sumneko_lua添加到环境变量中
-- /home/user/.local/share/nvim/lsp_servers/sumneko_lua/extension/server/bin/
-- 将clangd添加到环境变量中
-- pacman -S clang
-- /home/user/.local/share/nvim/lsp_servers/clangd/clangd_13.0.0/bin
-- 安装bash-language-serve
-- npm i -g bash-language-server
local servers = { "pyright", "clangd", "bashls" }
for _, lsp in ipairs(servers) do
	nvim_lsp[lsp].setup({
		on_attach = on_attach,
		capabilities = capabilities,
		flags = {
			debounce_text_changes = 150,
		},
		root_dir = function()
			return vim.fn.getcwd()
		end,
	})
end

local luaconf = {
	capabilities = capabilities,
	on_attach = on_attach,
	-- 		cmd = {sumneko_binary, "-E", sumneko_root_path .. "/main.lua"},
	settings = {
		Lua = {
			runtime = {
				-- Tell the language server which version of Lua you're using (most likely LuaJIT in the case of Neovim)
				version = "LuaJIT",
				-- Setup your lua path
				path = vim.split(package.path, ";"),
			},
			diagnostics = {
				globals = { "vim", "dump" },
			},
			workspace = {
				-- Make the server aware of Neovim runtime files
				library = {
					[vim.fn.expand("$VIMRUNTIME/lua")] = true,
					[vim.fn.expand("$VIMRUNTIME/lua/vim/lsp")] = true,
				},
			},
			maxPreload = 100000,
			preloadFileSize = 10000,
		},
	},
}
nvim_lsp.sumneko_lua.setup(luaconf)

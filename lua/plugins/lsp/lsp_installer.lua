local status, lsp_installer = pcall(require, "nvim-lsp-installer")
if not status then
	vim.notify("Lsp-installer not found!", vim.log.levels.EEEOR)
	return
end

lsp_installer.settings({
	ui = {
		icons = {
			server_installed = "✓",
			server_pending = "➜",
			server_uninstalled = "✗",
		},
	},
	pip = {
		install_args = { "-i", "https://pypi.tuna.tsinghua.edu.cn/simple/" },
	},
})

local map = vim.api.nvim_buf_set_keymap
local opts = { noremap = true, silent = true }
vim.api.nvim_set_keymap("n", "<space>le", "<cmd>lua vim.diagnostic.open_float()<CR>", opts)
vim.api.nvim_set_keymap("n", "<space>lk", "<cmd>lua vim.diagnostic.goto_prev()<CR>", opts)
vim.api.nvim_set_keymap("n", "<space>lj", "<cmd>lua vim.diagnostic.goto_next()<CR>", opts)
vim.api.nvim_set_keymap("n", "<space>ll", "<cmd>lua vim.diagnostic.setloclist()<CR>", opts)
-- vim.api.nvim_set_keymap("n", "gp", ":lua require('plugins.lsp.utils.hover').hover()<cr>",opts)

return function(_, bufnr,client)
	-- Enable completion triggered by <c-x><c-o>
	vim.api.nvim_buf_set_option(bufnr, "omnifunc", "v:lua.vim.lsp.omnifunc")
	-- mappings
	map(bufnr, "n", "gD", "<cmd>lua vim.lsp.buf.declaration()<CR>", opts)
	map(bufnr, "n", "gd", "<cmd>lua vim.lsp.buf.definition()<CR>", opts)
	map(bufnr, "n", "K", "<cmd>lua vim.lsp.buf.hover()<CR>", opts)
	map(bufnr, "n", "<space>lgi", "<cmd>lua vim.lsp.buf.implementation()<CR>", opts)
	map(bufnr, "n", "<space>ls", "<cmd>lua vim.lsp.buf.signature_help()<CR>", opts)
	map(bufnr, "n", "<space>lD", "<cmd>lua vim.lsp.buf.type_definition()<CR>", opts)
	map(bufnr, "n", "<space>lr", "<cmd>lua vim.lsp.buf.rename()<CR>", opts)
	map(bufnr, "n", "<space>la", "<cmd>lua vim.lsp.buf.code_action()<CR>", opts)
	map(bufnr, "n", "<space>lgr", "<cmd>lua vim.lsp.buf.references()<CR>", opts)
  -- require("illuminate").on_attach(client)
end

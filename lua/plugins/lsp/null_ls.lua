local status, null_ls = pcall(require, "null-ls")
if not status then
	return print("Null-ls not found!")
end
null_ls.setup({
	sources = {
		-- install rust  换源  cargo install stylua
		null_ls.builtins.formatting.stylua,
		-- pip install black
		null_ls.builtins.formatting.black,
		-- install clang  pacman -S clang
		-- null_ls.builtins.formatting.clang_format,
		-- pacman -S shfmt
		null_ls.builtins.formatting.shfmt,
		-- pacman -S prettier
		null_ls.builtins.formatting.prettier,
		-- null_ls.builtins.diagnostics.luacheck,
	},
})

local load_packer = require("plugins.init-packer")
local base = require("plugins.base")
local tools = require("plugins.tools")
local completion = require("plugins.completion")
local lspconfig = require("plugins.lsp")

local packer = load_packer.packer
local first_install = load_packer.first_install

return packer.startup(function(use)
	use({ "wbthomason/packer.nvim", "lewis6991/impatient.nvim"})
	use({ "nvim-lua/plenary.nvim", "yianwillis/vimcdoc" })

	base(use)
	tools(use)
	completion(use)
	lspconfig(use)
	-- use("~/workstation/nvimPlugins/first.nvim")

	if first_install then
		packer.sync()
	end
end)

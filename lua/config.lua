local M = {}
M.basic, M.ui, M.plugins = {}, {}, {}

M.basic = {
  opration = {
    insert = true,
    window = true,
  }
}

M.ui = {
	theme = "onedark",
	-- theme = "onelight",
	options = {
		-- 需要终端支持透明
		transparency = true,
		italic_comment = false,
	},
}

M.plugins = {
  Comment = true,
  LuaSnip = true,
  bufferline = true,
  copilot = true,
  diffview = true,
  fidget = true,
  filetype = true,
  friendly_snippets = true,
  gitsigns = true,
  hop = true,
  indent_blankline = true,
  lsp_signature = true,
  lualine = true,
  markdown_preview = true,
  neoscroll = true,
  nui = true,
  null_ls = true,
  numb = true,
  nvim_autopairs = true,
  nvim_cmp = true,
  nvim_colorizer = true,
  nvim_cursorline = true,
  nvim_lsp_installer = true,
  nvim_lspconfig = true,
  nvim_spectre = true,
  nvim_tree = true,
  nvim_treesitter = true,
  nvim_treesitter_context = true,
  nvim_treesitter_refactor = true,
  nvim_ts_rainbow = true,
  stabilize = true,
  telescope = true,
  todo_comments = true,
  trouble = true,
  vim_startuptime = true,
  vim_surround = true,
  vim_visual_multi = true,
  vimcdoc = true,
  vista = true,
  which_key = true,
}


return M
